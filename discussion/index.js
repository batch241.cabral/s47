const txtFirstName = document.querySelector("#txt-first-name");
const txtlastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

txtlastName.addEventListener("keyup", (event) => {
    spanFullName.innerHTML = `${txtFirstName.value} ${txtlastName.value}`;
    console.log(event.target);
});
